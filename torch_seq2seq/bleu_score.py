# -*- coding: utf-8 -*-

"""
A simple python wrapper for the standard mult-bleu.perl 
script used in machine translation/captioning models.

References
     NeuralTalk (https://github.com/karpathy/neuraltalk)

"""
import data_utils
import config

ref = "./evaluation/references"
cand = "./evaluation/candidates"
#
# def make_testfile(sent, file, language):
#     "separate Chinese chars "
#     f = open(file, 'a')
#     if language == "en":
#         f.write(sent)
#     if language == 'cn':
#         str = ' '
#         f.write(str.join(list))

def exact_match(ref,cand):
    f_ref = open(ref, "r")
    f_cand = open(cand, "r")

    refs = f_ref.readlines()
    cands = f_cand.readlines()

    lenth = len(refs)

    fres = open(config.EXACT_MATCH_PATH, "r+")
    fres.truncate()
    fres.close()

    fres = open(config.EXACT_MATCH_PATH, "a")

    for ind in range(lenth):
        reference = refs[ind].split()
        candidate = cands[ind].split()[:-1]


        # print reference,candidate

        if len(reference) == len(candidate):
            for i in range(len(reference)):
                if reference[i] != candidate[i]:
                    fres.write("0\n")
                    break
                else:
                    pass
            fres.write("1\n")
        else:
            fres.write("0\n")
    fres.close()

    fres = open(config.EXACT_MATCH_PATH, "r")
    sum = 0
    line_count = 0
    for line in fres:
        line_count+=1
        sum += float(line)
    fres.close()

    print "Exact match rate is: " + str (sum/line_count)

def bluescore(ref, cand):
    fres = open(config.BLEU_PATH, "r+")
    fres.truncate()

    f_ref = open(ref,"r")
    f_cand = open(cand,"r")

    refs = f_ref.readlines()
    cands = f_cand.readlines()

    lenth = len(refs)
    print lenth

    for ind in range(lenth):

        #remove the EOS symbol at the end of a sent
        reference = refs[ind].replace("<EOS>",'')
        output = cands[ind].replace("<EOS>",'')
        # print reference, output
        with open('./evaluation/output', 'w') as output_file:
            output_file.write(reference)

        with open('./evaluation/reference', 'w') as reference_file:
            reference_file.write(output)

        from os import system
        system('./evaluation/multi-bleu.perl ./evaluation/reference < ./evaluation/output >> '+config.BLEU_PATH)





