# -*- coding: utf-8 -*-

train = False # True: training mode/ False: evaluation mode
use_cuda = False

SOS_token = 0
EOS_token = 1

MAX_LENGTH = 15
hidden_size = 256
TRAIN_ITERS = 100000
PRINT_EVERY = 1000
PLOT_EVERY = 100

num_to_eval = 100

# format for SRC and TAR: LAN(ATTRIBUTE)
SRC = "cn(cat-samp)"
TAR = "en(cat-samp)"

T_SRC = "cn1"
T_TAR = "en1"


ENCODER_PATH = "./models/encoder"
DECODER_PATH = "./models/decoder"

EXACT_MATCH_PATH = "./evaluation/exact_match"
BLEU_PATH = "./evaluation/bleu"



#
