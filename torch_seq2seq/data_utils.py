#coding=utf-8
import csv
# import utils, config
import codecs
from nltk import word_tokenize
import random



def make_testfile(sent, file, language):
    "separate Chinese chars "
    f = open(file, 'a')
    if language == "en":
        f.write(sent)
    if language == 'cn':
        f.write(u' '.join(tokenize_chinese(sent)).encode('utf-8').strip() + "\n")

def is_chinese(uchar):
    """判断一个unicode是否是汉字"""
    uchar = uchar.encode("utf-8")
    uchar = unicode(uchar, "utf8", errors="ignore")
    if uchar >= u'\u4e00' and uchar <= u'\u9fa5':
        return True
    else:
        return False


def utf2unicode(list):
    l = []
    for item in list:
        item = item.encode("utf-8")
        l.append(unicode(item, "utf8", errors="ignore"))
    return l


def tokenize_chinese(chinese):
    tokens = []
    tempstr = ""
    chars = chinese
    for i in range(len(chars)):
        if is_chinese(chars[i]):
            if tempstr != "":
                tempstr = tempstr.strip().strip('"').split(" ")
                tokens += tempstr
                tempstr = ""
            tokens.append(chars[i])
        else:
            tempstr += chars[i]
            if i == len(chars) - 1:
                tempstr = tempstr.strip().strip('"').split(" ")
                tokens += tempstr
    return tokens


def generate_random_test_file(total, test, num):
    count = 1
    f = open(total, 'r')
    ftst = open('./data/cn1-en1.txt','w')
    inds = random.sample(range(1,6419324), 100)

    for l in f:
        if count in inds:
              ftst.write(l)
        count += 1
    print count


ENPATH = "./data/en1.csv"
CNPATH = "./data/cn1.csv"

# generate_random_test_file('/Users/wangxu/Desktop/torch/data/cn(cat-all)-en(cat-all).txt', 0, 10)